package com.example.s_e_s.phonebook;


import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private TabHost tabHost;
    private EditText etName,etSurname,etPhoneNumber,etEmail,etBirthday,etNametoDel,etSurnametoDel;
    private ListView listView;
    private String path;
    private Spinner spinner;
    private SQLiteDatabase database = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Tab Host
        tabHost = (TabHost) findViewById(R.id.tabhost);
        tabHost.setup();
        TabHost.TabSpec tabSpec;

        //Database SQLite
        File myDbPath = getApplication().getFilesDir();
        path = myDbPath + "/"+"CMPE409";


        //Tab 1 Contents
        etName = (EditText) findViewById(R.id.etName);
        etSurname = (EditText) findViewById(R.id.etSurname);
        etPhoneNumber = (EditText) findViewById(R.id.etPhoneNumber);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etBirthday = (EditText) findViewById(R.id.etBirthday);
        spinner = (Spinner) findViewById(R.id.spinner);


        //Tab 2 Contents
        listView = (ListView) findViewById(R.id.listView);
        etNametoDel = (EditText) findViewById(R.id.etNametoDel);
        etSurnametoDel = (EditText) findViewById(R.id.etSurnametoDel);

        //Tab 1
        tabSpec = tabHost.newTabSpec("tab1");
        tabSpec.setContent(R.id.tab1);
        tabSpec.setIndicator("Add New Person", null);
        tabHost.addTab(tabSpec);

        //Tab2
        tabSpec = tabHost.newTabSpec("tab2");
        tabSpec.setContent(R.id.tab2);
        tabSpec.setIndicator("Contacts",null);
        tabHost.addTab(tabSpec);

        String[] arraySpinner=new String[]{"gmail.com","hotmail.com"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinner.setAdapter(adapter);

        try {


            if (!databaseExist()) {


                database = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.CREATE_IF_NECESSARY);
                Toast.makeText(this, "We have database", Toast.LENGTH_SHORT).show();

                String mytable = "create table person ( "
                        + "name text, "
                        + "surname text,"
                        + "phonenumber text, "
                        + "email text, "
                        + "birthday text);";
                database.execSQL(mytable); //we have table
                Toast.makeText(this, "We have table", Toast.LENGTH_SHORT).show();

            }
            else {
                Toast.makeText(this, "We have a database already", Toast.LENGTH_SHORT).show();
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
            Toast.makeText(this, "Error while creating database", Toast.LENGTH_SHORT).show();
        }

    }

    private boolean databaseExist() {
        File dbFile = new File(path);
        return dbFile.exists();
    }

    public void Read(View v) {
        database = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.CREATE_IF_NECESSARY);
        String readData = "select * from person";
        Cursor cursor = database.rawQuery(readData, null);
        ArrayList<String> tableData = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, tableData);
        while (cursor.moveToNext()) {
            String name = cursor.getString(cursor.getColumnIndex("name"));
            String lastName = cursor.getString(cursor.getColumnIndex("surname"));
            String phoneNumber = cursor.getString(cursor.getColumnIndex("phonenumber"));
            String email = cursor.getString(cursor.getColumnIndex("email"));
            String birthday = cursor.getString(cursor.getColumnIndex("birthday"));
            String result = "- Name: "+name+" "+lastName+" "
                    +"\n- Number: "+phoneNumber
                    +"\n- e-mail: "+email
                    +"\n- Birthday: "+birthday;
            tableData.add(result);
        }
        listView.setAdapter(adapter);
        database.close();
    }

    public void Write(View v) {
        try {
            if (!etName.getText().toString().isEmpty() && !etSurname.getText().toString().isEmpty() &&
                    !etPhoneNumber.getText().toString().isEmpty()) {
                database = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.CREATE_IF_NECESSARY);
                String name = etName.getText().toString();
                String lastName = etSurname.getText().toString();
                String phoneNumber = etPhoneNumber.getText().toString();
                String email = etEmail.getText().toString();
                String adres = spinner.getSelectedItem().toString();
                String birthday = etBirthday.getText().toString();
                String input = "insert into person (name, surname, phonenumber, email, birthday) values " +
                        "('" + name + "','" + lastName + "','" + phoneNumber + "','" + email+"@"+adres + "','" + birthday + "')";
                database.execSQL(input);
                Toast.makeText(this, "New Person Inserted", Toast.LENGTH_LONG).show();
                database.close();
                Read(v);
            }
            else
                throw new SQLException();
        }
        catch (SQLException e) {
            Toast.makeText(this, "Error while inserting, Please fill all the empty spaces", Toast.LENGTH_LONG).show();
        }


    }
    public void Delete(View v){
        try{
            if (!etNametoDel.getText().toString().isEmpty() && !etSurnametoDel.getText().toString().isEmpty()) {
                database = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.CREATE_IF_NECESSARY);
                String name = etNametoDel.getText().toString();
                String surname = etSurnametoDel.getText().toString();
                String delete = " delete from person where name = '" + name + "' AND surname = '" + surname + "'";
                database.execSQL(delete);
                Toast.makeText(this, name +" "+surname+" is deleted from the database", Toast.LENGTH_LONG).show();
                database.close();
                etNametoDel.setText("");
                etSurnametoDel.setText("");
                Read(v);
            }
            else {
                Toast.makeText(this, "Fill the empty spaces", Toast.LENGTH_SHORT).show();
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }

    }

    public void addClicked (View v){
        Write(v);
        Read(v);
        etName.setText("");
        etSurname.setText("");
        etPhoneNumber.setText("");
        etEmail.setText("");
        etBirthday.setText("");
    }
    public void updateClicked (View v){
        Read(v);
    }
}
